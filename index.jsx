// bài 1
document.getElementById("btn-sap-xep").addEventListener("click", function () {
    var so1Value = Number(document.getElementById("txt-1").value);
    var so2Value = Number(document.getElementById("txt-2").value)
    var so3Value = Number(document.getElementById("txt-3").value)

    var so1, so2, so3;
    if (so1Value < so2Value && so1Value < so3Value) {
        if (so2Value < so3Value) {
            so1 = so1Value
            so2 = so2Value
            so3 = so3Value
        } else {
            so1 = so1Value
            so2 = so3Value
            so3 = so2Value
        }
        console.log("trườnG hợp 1")
    }

    if (so2Value < so1Value && so2Value < so3Value) {
        if (so1Value < so3Value) {
            so1 = so2Value
            so2 = so1Value
            so3 = so3Value
        } else {
            so1 = so2Value
            so2 = so3Value
            so3 = so1Value
        }
        console.log("trườnG hợp 2")
    }

    if (so3Value < so1Value && so3Value < so2Value) {
        if (so1Value < so2Value) {
            so1 = so3Value
            so2 = so1Value
            so3 = so2Value
        } else {
            so1 = so3Value
            so2 = so2Value
            so3 = so1Value
        }
        console.log("trườnG hợp 3")
    }
    var result = (so1 + "<" + so2 + "<" + so3);
    console.log("result:", result);
    document.getElementById("resultSapxep").innerHTML = result
})



// bài 2
document.getElementById("btn-xin-chao").addEventListener("click", function () {
    var boValue = document.getElementById("inputGroupSelect01").value;
    var text = "Xin Chào "
    switch (boValue) {
        case "B":
            text += "bố"
            break;
        case "M":
            text += "mẹ"
            break;
        case "A":
            text += "anh trai"
            break;
        case "E":
            text += "em gái"
            break;
    }
    document.getElementById("resultHello").innerHTML = text
})



//bài 3 
document.getElementById("btn-dem").addEventListener("click", function () {

    var nhapSo1value = document.getElementById("txt-nhap-so-1").value * 1;
    var nhapSo2value = document.getElementById("txt-nhap-so-2").value * 1;
    var nhapSo3value = document.getElementById("txt-nhap-so-3").value * 1;


    var countSoChan = 0;
    var countSoLe = 0;


    if (nhapSo1value % 2 == 0) {
        countSoChan++;
    } else {
        countSoLe++;
    }

    if (nhapSo2value % 2 == 0) {
        countSoChan++;
    } else {
        countSoLe++;
    }

    if (nhapSo3value % 2 == 0) {
        countSoChan++;
    } else {
        countSoLe++;
    }

    console.log(countSoChan, countSoLe);

    document.getElementById("resultDem").innerHTML = `<span>Có ${countSoChan} số chẵn, ${countSoLe} số lẻ</span>`
});


//bài 4
document.getElementById("btn-du-doan").addEventListener("click", function () {
    var doDai1value = document.getElementById("txt-do-dai-1").value * 1;
    var doDai2value = document.getElementById("txt-do-dai-2").value * 1;
    var doDai3value = document.getElementById("txt-do-dai-3").value * 1;



    if (doDai1value == doDai2value && doDai1value == doDai3value && doDai2value == doDai3value) {
        console.log("tam giác đều");
        document.getElementById("resultDuDoan").innerHTML = `<span> Tam Giác Đều </span>`
    }
    else if (doDai1value == doDai3value || doDai1value == doDai2value
        || doDai2value == doDai1value || doDai2value == doDai3value
        || doDai3value == doDai1value || doDai3value == doDai2value) {
        console.log("tam giác cân");
        document.getElementById("resultDuDoan").innerHTML = `<span> Tam Giác Cân </span>`

    } else {
        const result = ((doDai1value * doDai1value + doDai2value * doDai2value) / doDai3value) == doDai3value
        const result1 = ((doDai3value * doDai3value + doDai2value * doDai2value) / doDai1value) == doDai1value
        const result2 = ((doDai1value * doDai1value + doDai3value * doDai3value) / doDai2value) == doDai2value
        if (result || result1 || result2) {
            console.log("tam giác vuông");
            document.getElementById("resultDuDoan").innerHTML = `<span> Tam Giác Vuông </span>`

        } else {
            console.log("một tam giác nào đó");
            document.getElementById("resultDuDoan").innerHTML = `<span> Một Tam Giác Nào Đó </span>`

        }

    }
});
